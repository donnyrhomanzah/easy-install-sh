
#!/bin/sh

echo "===================================================================="
echo "-- REMOVE PREVIOUS DOCKER --"
echo "===================================================================="
sudo apt-get remove -y docker docker-engine docker.io containerd runc;

echo "===================================================================="
echo "-- INSTALL REQUIREMENTS --"
echo "===================================================================="
sudo apt-get update;
sudo apt-get install -y ca-certificates curl gnupg lsb-release;

echo "===================================================================="
echo "-- ADD GPG KEYS --"
echo "===================================================================="
sudo mkdir -p /etc/apt/keyrings;
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg;
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null;

echo "===================================================================="
echo "-- INSTALL DOCKER --"
echo "===================================================================="
sudo apt-get update;
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin;

echo "===================================================================="
echo "-- REGISTERING USER TO DOCKER --"
echo "===================================================================="
sudo usermod -aG docker $USER;

echo "===================================================================="
echo "-- CHECK DOCKER VERSION --"
echo "===================================================================="
docker version

echo "===================================================================="
echo "-- ADD DOCKER COMPOSE --"
echo "===================================================================="
curl -SL https://github.com/docker/compose/releases/download/v2.12.2/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose;
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose;
chmod +x /usr/local/bin/docker-compose;
which docker-compose
